FROM python:3.13
COPY --from=ghcr.io/astral-sh/uv:latest /uv /uvx /bin/

WORKDIR /app
COPY ./uv.lock ./pyproject.toml ./
ENV PYTHONPATH=/app
COPY README.md /app/
RUN uv sync --frozen

COPY snapper /app/snapper
COPY gql /app/gql

# Download the schema file
RUN curl -o schema.graphql https://gitlab.com/c2-games/scoring/hasura/-/raw/main/hasura/schemas/snapshot_service.graphql?ref_type=heads

# Generate the graphql_client directory
RUN uv run python -m ariadne_codegen

# Start running snapshots
CMD ["uv", "run", "snapper"]
