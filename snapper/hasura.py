from typing import List

from pydantic import BaseModel, PrivateAttr, model_validator
from typing_extensions import Self
from yaoasdf import DecodeOptions, OidcClient, OidcProvider, OidcHttpClient

from graphql_client.client import Client  # isort:skip pylint: disable=import-error


class Hasura(BaseModel):
    hasura_endpoint: str
    query_role: str | None = None
    hasura_admin_secret: str | None = None
    oidc_issuer_url: str | None = None
    oidc_client: str | None = None
    oidc_client_secret: str | None = None
    oidc_scopes: List[str] | None = None
    _client: Client | None = PrivateAttr(None)

    @model_validator(mode="after")
    def check_authentication(self) -> Self:
        """Validate that all necessary authentication requirements have been provided."""
        oidc_settings = (
            self.oidc_client,
            self.oidc_client_secret,
            self.oidc_issuer_url,
        )
        if any(oidc_settings) and not all(oidc_settings):
            raise ValueError(
                "all or none oidc_{client,client_secret,issuer_url} settings must be provided"
            )
        return self

    @property
    def client(self) -> Client:
        """GraphQL client"""
        if self._client:
            return self._client
        headers = {}
        if self.query_role:
            headers["x-hasura-role"] = self.query_role
        if self.oidc_client and self.oidc_client_secret and self.oidc_issuer_url:
            http_client = OidcHttpClient(
                headers=headers,
                oidc_client=OidcClient(
                    provider=OidcProvider(
                        issuer_url=self.oidc_issuer_url,
                        options=DecodeOptions(
                            verify_signature=True,
                            verify_exp=True,
                        ),
                    ),
                    client_id=self.oidc_client,
                    client_secret=self.oidc_client_secret,
                ),
            )
            self._client = Client(
                url=self.hasura_endpoint, headers=headers, http_client=http_client
            )
            return self._client
        elif self.hasura_admin_secret:
            headers["x-hasura-admin-secret"] = self.hasura_admin_secret

        self._client = Client(self.hasura_endpoint, headers)
        return self._client
