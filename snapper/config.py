"""Snapper configuration."""

from datetime import timedelta
from typing import Any, Optional

from cincoconfig import Config, Field, Schema, validator
from cincoconfig.fields import (
    ListField,
    LogLevelField,
    SecureField,
    StringField,
    UrlField,
)
from pytimeparse2 import parse as timeparse


class DurationField(Field):
    """
    A human readable duration field. Values are validated that they parse to timedelta.
    """

    storage_type = timedelta

    def __init__(self, **kwargs):
        """
        Override to make sure default is valid
        """
        value = kwargs.get("default")
        if value is not None:
            value = self._convert(value)
            kwargs["default"] = value
        super().__init__(**kwargs)

    def _convert(self, value: Any) -> timedelta:
        if isinstance(value, timedelta):
            return value
        if isinstance(value, str):
            parsed = timeparse(value)
            if parsed is None:
                raise ValueError("value can not be parsed by any format")
            return timedelta(seconds=float(parsed))
        if isinstance(value, float):
            return timedelta(seconds=value)
        if isinstance(value, int):
            return timedelta(seconds=float(value))

        raise ValueError(
            f"value must be timedelta, str or number, not {type(value).__name__}"
        )

    def _validate(self, cfg: Config, value: Any) -> timedelta:
        return self._convert(value)

    def to_basic(self, cfg: Config, value: timedelta) -> str:
        return str(value)

    def to_python(self, cfg: Config, value: str) -> Optional[timedelta]:
        return self._convert(value)


schema = Schema(env="SNAPPER")
schema.auth.hasura_admin_secret = SecureField(required=False)
schema.auth.oidc_client = StringField(required=False)
schema.auth.oidc_client_secret = SecureField(required=False)
schema.auth.oidc_scopes = ListField(field=StringField(), default=lambda: [])
schema.hasura.url = UrlField(required=True)
schema.hasura.role = StringField(default="snapshot_service")
schema.interval = DurationField(default="5m")
schema.environment = StringField(required=True)
schema.log_level = LogLevelField(default="info")


config = schema()


def get_config() -> Config:
    """Get the validated config."""
    config.validate()
    return config


@validator(schema)
def validate_auth(cfg):
    """Validates that appropriate auth is specified."""
    if not cfg.auth.hasura_admin_secret:
        if not (cfg.auth.oidc_client and cfg.auth.oidc_client_secret):
            raise ValueError(
                "auth.hasura_admin_secret or auth.oidc_client and auth.oidc_client_secret must be specified"
            )


if __name__ == "__main__":
    config.validate()
    print(config.dumps(format="json", pretty=True, sensitive_mask=None).decode())
