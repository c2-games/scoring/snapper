"""Snapshot event data periodically"""

import logging
import uuid
from datetime import UTC, datetime, timedelta
from typing import Dict, List, Optional, Tuple

from pydantic import BaseModel

from snapper.hasura import Hasura
from snapper.config import get_config

from graphql_client.client import (
    EnvironmentEvent,
    EventSnapshot,
    wrapup_snapshots_insert_input,
)
from graphql_client.input_types import (
    wrapup_snapshots_bonus_arr_rel_insert_input,
    wrapup_snapshots_bonus_insert_input,
    wrapup_snapshots_service_arr_rel_insert_input,
    wrapup_snapshots_service_insert_input,
)

config = get_config()

# set logger to debug, let the handlers filter out what they want
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class TeamServicePointsEntry(BaseModel):
    status: str
    successful_team_checks: int = 0
    max_checks: int = 0
    score: float = 0
    max_points: float = 0
    total_team_checks: int = 0


class Snapper(BaseModel):
    hasura: Hasura

    @staticmethod
    def _summarize_service_points_build_return(
        team_service_points: Dict[str, Dict[str, TeamServicePointsEntry]],
    ) -> Tuple[float, Dict[str, List[wrapup_snapshots_service_insert_input]]]:
        ret: Dict[str, List[wrapup_snapshots_service_insert_input]] = {}
        # Track the highest points per service
        current_max: Dict[str, float] = {}
        # Calculate max service points by getting total team checks (ttc)
        # per service, then using (max_points / max_checks * ttc)
        for _, v in team_service_points.items():
            for check_id, check_stats in v.items():
                try:
                    value = check_stats.total_team_checks * (
                        check_stats.max_points / check_stats.max_checks
                    )
                except ZeroDivisionError:
                    value = 0.0
                current_max[check_id] = max(
                    current_max.get(check_id, 0),
                    value,
                )
        service_max = 0.0
        # Sum together the maximum for all checks
        for _, e in current_max.items():
            service_max = e + service_max
        # Build the service snapshot information for each team
        for team_id, i in team_service_points.items():
            snap = ret.get(team_id, [])
            for check_id, stats in i.items():
                try:
                    max_score = stats.total_team_checks * (
                        stats.max_points / stats.max_checks
                    )
                except ZeroDivisionError:
                    max_score = 0.0
                snap.append(
                    wrapup_snapshots_service_insert_input(
                        check_id=check_id,
                        status=stats.status,
                        accrued_checks=stats.successful_team_checks,
                        max_checks=stats.total_team_checks,
                        score=stats.score,
                        max_score=max_score,
                    )
                )
            ret[team_id] = snap
        return (service_max, ret)

    @staticmethod
    def _summarize_service_points(
        data: EventSnapshot,
    ) -> Tuple[float, Dict[str, List[wrapup_snapshots_service_insert_input]]]:
        team_service_points: Dict[str, Dict[str, TeamServicePointsEntry]] = {}
        for entry in data.scoreboard_team_summary:
            if not (entry and isinstance(entry.team_id, str)):
                raise RuntimeError("error parsing team summary data")
            team_services: Dict[str, TeamServicePointsEntry] = {}
            for check in entry.service_status:
                if not (
                    check
                    and isinstance(check.check_id, str)
                    and isinstance(check.result_code, str)
                ):
                    raise RuntimeError("error parsing summary check data")
                team_services[check.check_id] = TeamServicePointsEntry(
                    status=check.result_code
                )
            team_service_points[entry.team_id] = team_services
        # Add together all check stats from all periods
        for sps_entry in data.scoring_service_period_score:
            tsp_entry: Dict[str, TeamServicePointsEntry] = team_service_points[
                sps_entry.team_id
            ]
            stats: TeamServicePointsEntry = tsp_entry[sps_entry.check_id]
            tsp_entry[sps_entry.check_id] = TeamServicePointsEntry(
                status=stats.status,
                successful_team_checks=stats.successful_team_checks
                + sps_entry.successful_team_checks,
                max_checks=stats.max_checks + sps_entry.max_checks,
                score=stats.score + sps_entry.score,
                max_points=stats.max_points + sps_entry.max_points,
                total_team_checks=stats.total_team_checks + sps_entry.total_team_checks,
            )
            team_service_points[sps_entry.team_id] = tsp_entry
        return Snapper._summarize_service_points_build_return(team_service_points)

    @staticmethod
    def _summarize_bonus_points(
        data: EventSnapshot,
    ) -> Tuple[float, Dict[str, List[wrapup_snapshots_bonus_insert_input]]]:
        ret: Dict[str, List[wrapup_snapshots_bonus_insert_input]] = {}
        bonus_max = 0.0
        for team_entry in data.scoreboard_team_summary:
            if not (team_entry and isinstance(team_entry.team_id, str)):
                raise RuntimeError("error parsing team summary bonus scores")
            bonus_points = []
            team_bonus_max = 0.0
            if team_entry.score is None:
                raise RuntimeError("error parsing team bonus scores")
            for bonus in team_entry.score.bonus_by_category:
                bonus_points.append(
                    wrapup_snapshots_bonus_insert_input(
                        category=bonus.category,
                        score=bonus.score,
                    )
                )
                team_bonus_max = team_bonus_max + (bonus.score or 0.0)
            # Track the max bonus score seen yet
            bonus_max = max(team_bonus_max, bonus_max)
            ret[team_entry.team_id] = bonus_points
        return (bonus_max, ret)

    @staticmethod
    def _format_snapshot_summary(
        data: EventSnapshot,
    ) -> List[wrapup_snapshots_insert_input]:
        ret = []
        now = datetime.now().isoformat()
        snapshot_id = uuid.uuid4()  # Generate a uuid for the snapshots
        # Check for malformed data
        if (
            data.challenges_active_aggregate.aggregate is None
            or data.challenges_active_aggregate.aggregate.sum is None
            or data.challenges_active_aggregate.aggregate.sum.points is None
        ):
            raise RuntimeError("malformed snapshot data")
        ctf_total_points = data.challenges_active_aggregate.aggregate.sum.points
        ctf_total_released = data.challenges_active_aggregate.aggregate.count
        bonus_max, bonus_points = Snapper._summarize_bonus_points(data)
        service_max, service_points = Snapper._summarize_service_points(data)
        for team_entry in data.scoreboard_team_summary:
            # Check for malformed data
            if (
                team_entry.score is None
                or team_entry.challenge_solves_aggregate.aggregate is None
                or team_entry.team_id is None
            ):
                raise RuntimeError("malformed team snapshot data")
            team_snapshot = wrapup_snapshots_insert_input(
                id=snapshot_id,
                event_id=team_entry.event_id,
                team_id=team_entry.team_id,
                time=now,
                total_score=team_entry.score.total,
                services_score=team_entry.score.service,
                ctf_score=team_entry.score.ctf,
                ctf_challenge_count=team_entry.challenge_solves_aggregate.aggregate.count,
                ctf_challenge_available=ctf_total_released,
                max_total_score=bonus_max + ctf_total_points + service_max,
                max_service_score=service_max,
                max_ctf_score=ctf_total_points,
            )
            if bonus_points.get(team_entry.team_id):
                team_snapshot.bonus_scores = (
                    wrapup_snapshots_bonus_arr_rel_insert_input(
                        data=bonus_points[team_entry.team_id],
                    )
                )
            if service_points.get(team_entry.team_id):
                team_snapshot.services = wrapup_snapshots_service_arr_rel_insert_input(
                    data=service_points[team_entry.team_id],
                )
            ret.append(team_snapshot)
        return ret

    async def take_snapshot(self, event_id: str):
        """Take a snapshot for an event."""
        result: EventSnapshot = await self.hasura.client.event_snapshot(
            event_id=event_id
        )
        snapshot = Snapper._format_snapshot_summary(result)
        await self.hasura.client.insert_snapshot(snapshot)

    async def get_active_event(
        self, environment: str, end_grace: timedelta
    ) -> Optional[Tuple[str, str]]:
        """
        Get the active event for a given environment.

        Allow a grace period after the end to start snapshots.
        """
        result: EnvironmentEvent = await self.hasura.client.environment_event(
            environment=environment
        )
        if len(result.environments) == 0:
            logger.info(f"no environment available by name: {environment}")
            return None
        elif len(result.environments) > 1:
            logger.warning(
                f"multiple environments returned for name: {environment}; using first"
            )

        event = result.environments[0].event
        if not event.started:
            logger.warning(f"event {event.name} ({event.id}) has not started yet")
            return None
        elif event.active:
            logger.info(f"event {event.name} ({event.id}) is active")
        elif event.ended and event.end:
            now = datetime.now(UTC)
            end = datetime.fromisoformat(event.end)
            delta = now - end
            if delta > end_grace:
                logger.debug(
                    f"event {event.name} ({event.id}) has ended and is beyond grace period"
                )
                return None
            else:
                logger.info(
                    f"event {event.name} ({event.id}) has ended and is within grace period"
                )

        return (event.id, event.name)
