"""Snapshot event data periodically"""

import asyncio
import datetime
import logging

from scheduler.asyncio import Scheduler

from snapper.hasura import Hasura
from snapper.config import get_config
from snapper import Snapper, logger

from graphql_client.client import EnvironmentAuthSettings

# setup snapper
config = get_config()

# setup logging to console
formatter = logging.Formatter(
    "%(asctime)s %(name)s %(levelname)s: %(message)s", datefmt="%Y-%m-%dT%H:%M:%S%z"
)
# add console output
ch = logging.StreamHandler()
ch.setFormatter(formatter)
ch.setLevel(config.log_level.upper())
logger.addHandler(ch)


async def run_snapshot(snapper: Snapper):
    if not snapper:
        logger.fatal("snapper was never initialized")
        return
    active_event = await snapper.get_active_event(
        config.environment, config.interval * 2
    )
    if not active_event:
        logger.info("no active event found, skipping snapshot")
        return
    event_id, name = active_event
    logger.info(f"starting snapshot for {name} ({event_id})")
    await snapper.take_snapshot(event_id)
    logger.info(f"finished snapshot for {name} ({event_id})")


async def _run():
    if config.auth.hasura_admin_secret:
        snapper = Snapper(
            hasura=Hasura(
                hasura_endpoint=config.hasura.url,
                hasura_admin_secret=config.auth.hasura_admin_secret,
                query_role=config.hasura.role,
            )
        )
    else:
        unauthenticated_hasura = Hasura(
            hasura_endpoint=config.hasura.url, query_role="anonymous"
        )
        result: EnvironmentAuthSettings = (
            await unauthenticated_hasura.client.environment_auth_settings(
                environment=config.environment
            )
        )
        if len(result.environments) == 0:
            logger.error(f"no environment available by name: {config.environment}")
            return None
        elif len(result.environments) > 1:
            logger.warning(
                f"multiple environments returned for name: {config.environment}; using first"
            )
        settings = result.environments[0].settings
        if not settings:
            logger.error(f"settings are null for environment: {config.environment}")
            return None
        # Setup snapper for the rest of the tasking
        snapper = Snapper(
            hasura=Hasura(
                hasura_endpoint=config.hasura.url,
                query_role=config.hasura.role,
                oidc_issuer_url=settings.keycloak_url,
                oidc_client=config.auth.oidc_client,
                oidc_client_secret=config.auth.oidc_client_secret,
                oidc_scopes=config.auth.oidc_scopes,
            )
        )

    def _run_snapshot():
        return run_snapshot(snapper)

    schedule = Scheduler()
    # Create a snapshot immediately
    schedule.once(datetime.timedelta(seconds=1), _run_snapshot)
    schedule.cyclic(config.interval, _run_snapshot)

    while True:
        await asyncio.sleep(1)


asyncio.run(_run())
