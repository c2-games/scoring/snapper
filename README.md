# Snapper

Periodically snapshot competition data and store it in the database.

## Getting started

### Configuration

Make a file at `.env` to contain your configuration.

```sh
# AUTH_OIDC_CLIENT is the OIDC confidential client name
# mutually exclusive with HASURA_ADMIN_SECRET
SNAPPER_AUTH_OIDC_CLIENT="snapshot-service"
# AUTH_OIDC_CLIENT_SECRET is the OIDC confidential client secret
# mutually exclusive with HASURA_ADMIN_SECRET
SNAPPER_AUTH_OIDC_CLIENT_SECRET=SuperS3cret
# AUTH_OIDC_SCOPES is the OIDC scopes to request with the token
SNAPPER_AUTH_OIDC_SCOPES="openid"
# HASURA_ADMIN_SECRET is the admin secret configured for hasura
# mutually exclusive with AUTH_OIDC_CLIENT and AUTH_OIDC_CLIENT_SECRET
SNAPPER_AUTH_HASURA_ADMIN_SECRET=myadminsecretkey
# HASURA_URL is the graphql endpoint of the hasura instance used to look up
# information for validating query/mutation inputs
SNAPPER_HASURA_URL=http://localhost:8080/v1/graphql
# HASURA_ROLE is the role used for queries and mutations
SNAPPER_HASURA_ROLE="snapshot_service"
# INTERVAL is the time to wait between snapshots
SNAPPER_INTERVAL="5m"
# ENVIRONMENT is the environment to source the event to run snapshots against
SNAPPER_ENVIRONMENT="local-dev"
# LOG_LEVEL filter output by increasing log level
SNAPPER_LOG_LEVEL="info"
```

### Running

```sh
# Source your .env file if needed
. ./.env
# Download the schema file
curl -o schema.graphql https://gitlab.com/c2-games/scoring/hasura/-/raw/main/hasura/schemas/snapshot_service.graphql?ref_type=heads
# Generate the graphql_client directory
uv run python -m ariadne_codegen
# Run the snapshot service
uv run snapper
```
